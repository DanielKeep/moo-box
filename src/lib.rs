/*!
Because an entire cow is big and unwieldy; we don't want the whole cow, just the moo.
*/
/*
Working kcov invocation:

    kcov --verify --include-pattern=/home target/cov target/debug/moo_box-*
*/
#![deny(missing_docs)]
#[cfg(test)] #[macro_use] extern crate assert_matches;

pub use ::impls::*;

pub mod traits;

use std::cmp::Ordering;
use std::fmt;
use std::hash;
use std::marker::PhantomData;
use std::mem;
use std::ops::Deref;
use ::internal::FatPtr;
use ::traits::{Owned, DstKind};

mod impls;
mod internal;

/**
A "Minimal-overhead, Optionally Owned" box for storing either borrowed or owned values.

The `Moo` type is useful when you want to store or pass around values which could be either owned or borrowed.  Unlike the related `Cow` type, a `Moo`, only consumes as much space as a standard fat pointer.  In addition, `Option<Moo<_>>` does not consume any additional space over just `Moo<_>`.

However, `Moo` does *not* support direct mutation (*c.f.* `Cow::to_mut`).  If you want to modify the contents of a `Moo`, use `Moo::into_owned` to get an owned value, mutate that, then turn it back into a `Moo::from_owned`.

For details on adding support for a type to `Moo`, see the documentation for the `Owned` and `DstKind` traits.

# Portability Notes

On some architectures, a `Moo` may require an additional (transparent) heap allocation when storing array-like values with a number of elements that meets or exceeds half the size of the architecture's address space.  For example, this is the case on 32-bit architectures when trying to store an array with `2**31` or more elements.

This does not apply to any known 64-bit architectures.
*/
pub struct Moo<'a, T: 'a + Owned> {
    // `&[()]` is used to trick the compiler into giving us `NonZero` semantics.
    storage: &'a [()],

    // Note that we also want to disallow `Send` and `Sync` by default.
    _marker: PhantomData<(&'a T::Borrowed, T, *const ())>,
}

#[test]
fn test_moo() {
    {
        let s = "Hello, world!";
        let m = MooStr::from(s);
        assert_eq!(&*m, s);
        assert_matches!(m.as_boo_ref(), BooRef::Borrowed(_));
    }
    {
        let s = "Hello, world!";
        let m = MooStr::from(String::from(s));
        assert_eq!(&*m, s);
        assert_matches!(m.into_boo(), Boo::Owned(_));
    }
    {
        let s = &[1, 2, 3][..];
        let m = MooVec::from(s);
        assert_eq!(&*m, s);
        assert_matches!(m.into_boo(), Boo::Borrowed(_));
    }
    {
        let s = &[1, 2, 3][..];
        let m = MooVec::from(Vec::from(s));
        assert_eq!(&*m, s);
        assert_eq!(m.into_owned(), Box::from(vec![1, 2, 3]));
    }
}

#[test]
fn test_moo_indirect() {
    if let Some(limit) = internal::ELEMENTS_CAUSING_INDIRECTION {
        {
            let data = vec![0x42u8; limit-1];
            {
                let m0 = MooVec::from(&*data);
                assert_eq!(m0.len(), limit-1);
            }
            let m1 = MooVec::from(data);
            assert_eq!(m1.len(), limit-1);
        }
        {
            let data = vec![0x42u8; limit];
            {
                let m0 = MooVec::from(&*data);
                assert_eq!(m0.len(), limit);
            }
            let m1 = MooVec::from(data);
            assert_eq!(m1.len(), limit);
        }
        {
            let data = vec![0x42u8; limit+1];
            {
                let m0 = MooVec::from(&*data);
                assert_eq!(m0.len(), limit+1);
            }
            let m1 = MooVec::from(data);
            assert_eq!(m1.len(), limit+1);
        }
    }
}

#[test]
fn test_moo_size() {
    use std::mem;
    assert_eq!(mem::size_of::<Moo<'static, Box<str>>>(), mem::size_of::<&[u8]>());
    assert_eq!(mem::size_of::<Moo<'static, Box<str>>>(), mem::size_of::<Option<Moo<'static, Box<str>>>>());
}

impl<'a, T: 'a + Owned> Moo<'a, T> {
    /// Create a `Moo` from a `Boo`.
    pub fn from_boo(boo: Boo<T>) -> Self {
        let fat = FatPtr::from_boo(boo);
        Moo {
            storage: fat.into_storage(),
            _marker: PhantomData,
        }
    }

    /**
    Create a `Moo` from a borrowed value.

    If type inference fails to determine the type of the result, you can specify it like so: `Moo::<Box<str>>::from_borrowed("moo!")`.
    */
    pub fn from_borrowed(v: &'a T::Borrowed) -> Moo<'a, T> {
        Self::from_boo(Boo::Borrowed(v))
    }

    /// Create a `Moo` from an owned value.
    pub fn from_owned(v: T) -> Moo<'a, T> {
        Self::from_boo(Boo::Owned(v))
    }

    /**
    Create a borrowed `BooRef`, providing immutable access to the contents of this `Moo`.

    A `BooRef` is a regular `enum`, meaning it can be easily deconstructed by `match` and other pattern-matching constructs.
    */
    pub fn as_boo_ref<'b>(&'b self) -> BooRef<'a, 'b, T> {
        unsafe {
            FatPtr::from_storage(self.storage).into_boo_ref::<'a, 'b, T>()
        }
    }

    /**
    Convert this `Moo` into a `Boo`.

    A `Boo` is a regular `enum`, meaning it can be easily deconstructed by `match` and other pattern-matching constructs.
    */
    pub fn into_boo(self) -> Boo<'a, T> {
        unsafe {
            let fat = FatPtr::from_storage(self.storage);
            mem::forget(self);
            fat.into_boo::<'a, T>()
        }
    }
}

impl<'a, T: 'a + Owned<Borrowed=U>, U: ?Sized + DstKind> Moo<'a, T>
where for<'b> &'b U: Into<T> {
    /**
    Convert this `Moo` into an owned value.

    If the `Moo` already contains an owned value, then this value is returned directly.  If the `Moo` contains a borrowed value, a new owned copy is allocated using `Into`.
    */
    pub fn into_owned(self) -> T {
        self.into_boo().into_owned()
    }
}

macro_rules! impl_fmt {
    ($t:ident) => {
        impl<'a, T: 'a + Owned> fmt::$t for Moo<'a, T>
        where
            T: fmt::$t,
            T::Borrowed: fmt::$t,
        {
            fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
                match self.as_boo_ref() {
                    BooRef::Borrowed(ptr) => ptr.fmt(fmt),
                    BooRef::Owned(ptr) => ptr.fmt(fmt),
                }
            }
        }
    };
}

impl<'a, T: 'a + Owned, U> AsRef<U> for Moo<'a, T>
where
    T::Borrowed: AsRef<U>,
    U: ?Sized,
{
    fn as_ref(&self) -> &U {
        self.deref().as_ref()
    }
}

#[test]
fn test_moo_as_ref() {
    {
        let m = MooStr::from("oh hi mark");
        assert_eq!(AsRef::<[u8]>::as_ref(&m), &b"oh hi mark"[..]);
    }
}

impl_fmt! { Binary }

impl<'a, T: 'a + Owned + Clone> Clone for Moo<'a, T> {
    fn clone(&self) -> Self {
        unsafe {
            let fat = FatPtr::from_storage(self.storage).clone_deep::<T>();
            Moo {
                storage: fat.into_storage(),
                _marker: PhantomData,
            }
        }
    }
}

#[test]
fn test_moo_clone() {
    {
        let s = "Hello, world!";
        let m0 = Moo::from(s);
        {
            let m1 = m0.clone();
            assert_eq!(&*m0, s);
            assert_eq!(&*m1, s);
        }
        assert_eq!(&*m0, s);
    }
    {
        let s = "Hello, world!";
        let m0 = Moo::from(String::from(s));
        {
            let m1 = m0.clone();
            assert_eq!(&*m0, s);
            assert_eq!(&*m1, s);
        }
        assert_eq!(&*m0, s);
    }
}

impl_fmt! { Debug }

impl<'a, T: 'a + Owned> Default for Moo<'a, T>
where T: Default {
    fn default() -> Self {
        Moo::from_owned(T::default())
    }
}

#[test]
fn test_moo_default() {
    let m = MooStr::default();
    assert_eq!(&*m, "");
    assert_matches!(m.as_boo_ref(), BooRef::Owned(_));
}

impl<'a, T: 'a + Owned> Deref for Moo<'a, T> {
    type Target = T::Borrowed;

    #[inline]
    fn deref(&self) -> &Self::Target {
        unsafe {
            FatPtr::from_storage(self.storage).as_ref::<T>()
        }
    }
}

impl_fmt! { Display }

#[test]
fn test_moo_display() {
    assert_eq!(format!("{}", MooStr::from("case")), String::from("case"));
}

impl<'a, T: 'a + Owned> Drop for Moo<'a, T> {
    fn drop(&mut self) {
        unsafe {
            drop(FatPtr::from_storage(self.storage).into_boo::<T>());
        }
    }
}

impl<'a, T: 'a + Owned> Eq for Moo<'a, T>
where T::Borrowed: Eq {}

impl<'a, T: 'a + Owned> PartialEq<Moo<'a, T>> for Moo<'a, T>
where T::Borrowed: PartialEq {
    fn eq(&self, other: &Moo<'a, T>) -> bool {
        self.deref().eq(other.deref())
    }
}

#[test]
fn test_moo_eq() {
    let s = "it's a mad, mad world";
    let m0 = Moo::from(s);
    let m1 = Moo::from(String::from(s));
    assert_eq!(m0, m1);
}

impl<'a, T: 'a + Owned> hash::Hash for Moo<'a, T>
where T::Borrowed: hash::Hash {
    fn hash<H>(&self, state: &mut H)
    where H: hash::Hasher {
        self.deref().hash(state)
    }
}

#[test]
fn test_moo_hash() {
    use std::collections::HashMap;

    let s = "browns are delicious";
    let mut map = HashMap::new();
    map.insert(MooStr::from(s), 1);
    map.insert(MooStr::from(String::from(s)), 2);

    assert_eq!(map.get(s), Some(&2));
}

impl<'a, T: 'a + Owned> Ord for Moo<'a, T>
where T::Borrowed: Ord {
    fn cmp(&self, other: &Self) -> Ordering {
        self.deref().cmp(other.deref())
    }
}

impl<'a, T: 'a + Owned> PartialOrd for Moo<'a, T>
where T::Borrowed: PartialOrd {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.deref().partial_cmp(other.deref())
    }
}

impl_fmt! { LowerExp }

impl_fmt! { LowerHex }

impl_fmt! { Octal }

impl_fmt! { Pointer }

unsafe impl<'a, T: 'a + Owned> Send for Moo<'a, T>
where
    T: Send,
    T::Borrowed: Sync,
{}

unsafe impl<'a, T: 'a + Owned> Sync for Moo<'a, T>
where
    T: Sync,
    T::Borrowed: Sync,
{}

impl_fmt! { UpperExp }

impl_fmt! { UpperHex }

/**
"Borrowed or Owned" is a helper type sometimes used as an "unpacked" representation of a `Moo`.
*/
#[derive(Clone, Debug)]
pub enum Boo<'a, T: 'a + Owned> {
    #[allow(missing_docs)]
    Borrowed(&'a T::Borrowed),

    #[allow(missing_docs)]
    Owned(T),
}

impl<'a, T: 'a + Owned<Borrowed=U>, U: ?Sized + DstKind> Boo<'a, T> {
    /**
    Convert this `Boo` into an owned value.

    If the `Boo` already contains an owned value, then this value is returned directly.  If the `Boo` contains a borrowed value, a new owned copy is allocated using `Into`.
    */
    pub fn into_owned(self) -> T
    where for<'b> &'b U: Into<T> {
        match self {
            Boo::Borrowed(ptr) => ptr.into(),
            Boo::Owned(ptr) => ptr,
        }
    }
}

/**
Like `Boo`, except the `Owned` variant contains an immutable borrow of the underlying owned value.
*/
#[derive(Clone, Debug)]
pub enum BooRef<'a: 'b, 'b, T: 'a + Owned> {
    #[allow(missing_docs)]
    Borrowed(&'a T::Borrowed),

    #[allow(missing_docs)]
    Owned(&'b T),
}
