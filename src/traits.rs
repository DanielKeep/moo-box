/*!
This module defines traits that you can implement to add `Moo` support.
*/

/**
This trait is used to enable `Moo` support for a type.

Note that incorrectly or inappropriately implementing this trait may lead to undefined behaviour.

# Implementation Requirements

- The subject type **must** be the same size as a "fat pointer"; *i.e.* two pointers wide.

- The subject type **must** be layout-compatible with "fat pointer"s; *i.e.* the first pointer-sized word must be a pointer, and the second word must be the "extra" data.

- The "extra" data **must** be one of the kinds listed in the `ExtraKind` enumeration.

`Owned` should only be implemented for *owned* types which are represented as a standard box of a dynamically-sized type.  For example: `Box<str>` and `Box<[T]>`.
*/
/*
- Why don't we just use `Deref` instead of requiring `Borrowed` and `owned_as_borrowed`?

  Because we *need* to include the `DstKind` constraint, which we can't readily do without having to repeat the constraint *everywhere*.

- Why don't we just replace the entire `Owned` trait with `Deref`?

  Because `Moo` is only compatible with a subset of all `Deref`-implementing types, and we don't have any way to express the requirements via existing traits.
*/
pub unsafe trait Owned {
    /**
    The borrowed equivalent for the subject type.

    This should be equivalent to `<Self as Deref>::Target`.

    For example: `<Box<str> as Owned>::Borrowed` is `str`.
    */
    type Borrowed: ?Sized + DstKind;

    /**
    Converts a borrow of the subject type into a borrow of the `Borrowed` type.

    This should be equivalent to `<Self as Deref>::deref(self)`.
    */
    fn owned_as_borrowed(&self) -> &Self::Borrowed;
}

/**
This trait is used to define what kind of dynamically sized type a `Owned::Borrowed` associated type is.

Note that incorrectly or inappropriately implementing this trait may lead to undefined behaviour.
*/
pub unsafe trait DstKind {
    /**
    Indicates what the "extra" data in a fat pointer to the subject is.

    Note that incorrectly or inappropriately implementing this function may lead to undefined behaviour.

    # Implementation Requirements

    - You **must** return `ExtraKind::Elements` **if and only if** the subject type's extra data is the number of elements from an array which is bounded by the largest physically contiguous amount of memory that can be allocated.

      Note that this is **not** the same as "the largest value representable by an integer of the same width as a pointer, which covers the entire address space".

      In particular, note that (as of the time of writing) all physical 64-bit architectures *do not* allow the use of their entire 64-bit address space.

    - You **must** return `ExtraKind::VTable` **if and only if** the subject type's extra data is a pointer to a vtable which **must** have an alignment greater-than or equal-to the natural alignment of a pointer **and** cannot be located at or between memory address zero, and the address equal to the natural alignment of a pointer.

      For example, on a 32-bit architecture, a vtable **must** have an alignment of at least 4 bytes, and cannot be located at or between memory addresses `0x0000_0000` and `0x0000_0004`.
    */
    fn extra_kind() -> ExtraKind;
}

/**
Denotes what kind of extra data a dynamically sized type attaches to fat pointers.
*/
pub enum ExtraKind {
    /**
    The extra data is the number of elements from an array, bounded by physical memory limits.
    */
    Elements,

    /**
    The extra data is a pointer to a vtable.
    */
    VTable,
}
