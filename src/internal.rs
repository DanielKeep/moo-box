use std::mem;
use ::{Boo, BooRef};
use ::traits::{Owned, DstKind, ExtraKind};

#[derive(Debug)]
pub(crate) struct FatPtr {
    ptr: *mut (),
    extra: usize,
}

impl FatPtr {
    #[inline]
    pub unsafe fn clone_deep<T: Owned + Clone>(&self) -> Self {
        let (unpacked, is_borrowed, _is_indirect) = self.clone_shallow().into_unpacked::<T>();
        if is_borrowed {
            return self.clone_shallow();
        }
        let ptr = mem::transmute::<&UnpackedFatPtr, &T>(&unpacked);
        FatPtr::from_owned((*ptr).clone())
    }

    #[inline]
    pub fn clone_shallow(&self) -> Self {
        FatPtr {
            ptr: self.ptr,
            extra: self.extra,
        }
    }

    #[inline]
    pub fn from_boo<T: Owned>(ptr: Boo<T>) -> FatPtr {
        match ptr {
            Boo::Borrowed(ptr) => FatPtr::from_borrowed::<T>(ptr),
            Boo::Owned(ptr) => FatPtr::from_owned(ptr),
        }
    }

    #[inline]
    pub fn from_borrowed<T: Owned>(ptr: &T::Borrowed) -> FatPtr {
        unsafe {
            let unpacked = if mem::size_of::<&T::Borrowed>() == mem::size_of::<FatPtr>() {
                mem::transmute_copy::<&T::Borrowed, UnpackedFatPtr>(&ptr)
            } else {
                unreachable!();
            };

            unpacked.into_fat::<T>(true)
        }
    }

    #[inline]
    pub fn from_owned<T: Owned>(ptr: T) -> FatPtr {
        unsafe {
            let unpacked = if mem::size_of::<T>() == mem::size_of::<FatPtr>() {
                mem::transmute_copy::<T, UnpackedFatPtr>(&ptr)
            } else {
                unreachable!();
            };
            mem::forget(ptr);
            unpacked.into_fat::<T>(false)
        }
    }

    #[inline]
    pub fn from_storage(storage: &[()]) -> Self {
        unsafe {
            mem::transmute(storage)
        }
    }

    #[inline]
    pub unsafe fn as_ref<'a, T: 'a + Owned>(&self) -> &'a T::Borrowed {
        let (unpacked, is_borrowed, _is_indirect) = self.clone_shallow().into_unpacked::<T>();
        match is_borrowed {
            true => {
                let ptr = mem::transmute_copy::<UnpackedFatPtr, &T::Borrowed>(&unpacked);
                ptr
            },
            false => {
                let ptr = mem::transmute::<&UnpackedFatPtr, &T>(&unpacked);
                ptr.owned_as_borrowed()
            },
        }
    }

    #[inline]
    pub unsafe fn into_boo<'a, T: 'a + Owned>(self) -> Boo<'a, T> {
        let (unpacked, is_borrowed, indirect) = self.into_unpacked::<T>();
        let boo = match is_borrowed {
            true => {
                let ptr = mem::transmute_copy::<UnpackedFatPtr, &T::Borrowed>(&unpacked);
                Boo::Borrowed(ptr)
            },
            false => {
                let ptr = mem::transmute_copy::<UnpackedFatPtr, T>(&unpacked);
                Boo::Owned(ptr)
            },
        };
        UnpackedFatPtr::destroy_indirect(indirect);
        boo
    }

    #[inline]
    pub(crate) unsafe fn into_boo_ref<'a: 'b, 'b, T: 'a + Owned>(self) -> BooRef<'a, 'b, T> {
        let (unpacked, is_borrowed, _is_indirect) = self.into_unpacked::<T>();
        match is_borrowed {
            true => {
                let ptr = mem::transmute_copy::<UnpackedFatPtr, &'a T::Borrowed>(&unpacked);
                BooRef::Borrowed(ptr)
            },
            false => {
                let ptr = mem::transmute::<&UnpackedFatPtr, &'b T>(&unpacked);
                BooRef::Owned(ptr)
            },
        }
    }

    #[inline]
    pub fn into_storage<'a>(self) -> &'a [()] {
        unsafe {
            mem::transmute(self)
        }
    }

    #[inline]
    unsafe fn into_unpacked<T: Owned>(self) -> (UnpackedFatPtr, bool, Indirect) {
        UnpackedFatPtr::from_fat::<T>(self)
    }
}

#[derive(Clone, Copy, Debug)]
struct UnpackedFatPtr {
    ptr: *mut (),
    extra: usize,

    /*
    Why not put `is_borrowed` here?

    Because we want to use `UnpackedFatPtr` directly with transmutes, at least for now.
    */
}

#[cfg(all(test, target_pointer_width = "32"))]
pub const ELEMENTS_CAUSING_INDIRECTION: Option<usize> = Some(0x4000_0000);

#[cfg(target_pointer_width = "32")]
type Indirect = Option<*mut UnpackedFatPtr>;

#[cfg(target_pointer_width = "32")]
impl UnpackedFatPtr {
    #[inline]
    unsafe fn destroy_indirect(indirect: Option<*mut UnpackedFatPtr>) {
        if let Some(indirect) = indirect {
            drop(Box::<UnpackedFatPtr>::from_raw(indirect));
        }
    }

    #[inline]
    fn unpack_extra<T: Owned>(extra: usize) -> (usize, bool, bool) {
        let (unpacked, flags) = match T::Borrowed::extra_kind() {
            ExtraKind::Elements => {
                const MASK: usize = !0 >> 2;
                (extra & MASK, (extra & !MASK) >> 30)
            },
            ExtraKind::VTable => (extra & !3, extra & 3),
        };
        let is_borrowed = (flags & 1) == 0;
        let is_indirect = (flags & 2) != 0;
        (unpacked, is_borrowed, is_indirect)
    }

    #[inline]
    unsafe fn from_fat<T: Owned>(fat: FatPtr) -> (UnpackedFatPtr, bool, Option<*mut UnpackedFatPtr>) {
        let (unpacked_extra, is_borrowed, is_indirect) = Self::unpack_extra::<T>(fat.extra);
        let (unpacked, indirect) = match is_indirect {
            false => (UnpackedFatPtr { ptr: fat.ptr, extra: unpacked_extra, }, None),
            true => {
                let indirect = fat.ptr as *mut UnpackedFatPtr;
                (*indirect, Some(indirect))
            },
        };
        (unpacked, is_borrowed, indirect)
    }

    #[inline]
    unsafe fn into_fat<T: Owned>(self, is_borrowed: bool) -> FatPtr {
        let (mask, shift) = match T::Borrowed::extra_kind() {
            ExtraKind::Elements => (!(!0 >> 2), 30),
            ExtraKind::VTable => (3, 0),
        };
        let is_indirect = match self.extra & mask {
            0 => false,
            _ => true,
        };

        let fat = match is_indirect {
            false => {
                let packed_extra = self.extra
                    | if is_borrowed { 0 } else { 1 << shift }
                    | /*if is_indirect*/ { 0 } /*else { 2 << shift }*/
                    ;
                FatPtr {
                    ptr: self.ptr,
                    extra: packed_extra,
                }
            },
            true => {
                let packed_extra = 0
                    | if is_borrowed { 0 } else { 1 << shift }
                    | /*if is_indirect { 0 } else*/ { 2 << shift }
                    ;
                let indirect = Box::new(UnpackedFatPtr {
                    ptr: self.ptr,
                    extra: self.extra,
                });
                let indirect = Box::into_raw(indirect) as *mut ();
                FatPtr {
                    ptr: indirect,
                    extra: packed_extra,
                }
            },
        };
        fat
    }
}

#[cfg(all(test, target_pointer_width = "64"))]
pub const ELEMENTS_CAUSING_INDIRECTION: Option<usize> = None;

#[cfg(target_pointer_width = "64")]
type Indirect = ();

#[cfg(target_pointer_width = "64")]
impl UnpackedFatPtr {
    #[inline]
    fn destroy_indirect(_: ()) {}

    #[inline]
    fn unpack_extra<T: Owned>(extra: usize) -> (usize, bool) {
        let (unpacked, flags) = match T::Borrowed::extra_kind() {
            ExtraKind::Elements => {
                const MASK: usize = !0 >> 1;
                (extra & MASK, extra & !MASK)
            },
            ExtraKind::VTable => (extra & !1, extra & 1),
        };
        let is_borrowed = flags == 0;
        (unpacked, is_borrowed)
    }

    #[inline]
    unsafe fn from_fat<T: Owned>(fat: FatPtr) -> (UnpackedFatPtr, bool, ()) {
        let (unpacked_extra, is_borrowed) = Self::unpack_extra::<T>(fat.extra);
        let unpacked = UnpackedFatPtr {
            ptr: fat.ptr,
            extra: unpacked_extra,
        };
        (unpacked, is_borrowed, ())
    }

    #[inline]
    unsafe fn into_fat<T: Owned>(self, is_borrowed: bool) -> FatPtr {
        let flag = match T::Borrowed::extra_kind() {
            ExtraKind::Elements => !0 >> 1,
            ExtraKind::VTable => !1,
        };
        debug_assert!(self.extra & !flag == 0);
        let packed_extra = self.extra | if is_borrowed { 0 } else { !flag };
        FatPtr {
            ptr: self.ptr,
            extra: packed_extra,
        }
    }
}
