use std::borrow::Borrow;
use std::cmp::Ordering;
use std::error::Error;
use ::Moo;
use ::traits::{Owned, DstKind, ExtraKind};

/**
A shorthand for `Moo<Box<str>>`.
*/
pub type MooStr<'a> = Moo<'a, Box<str>>;

unsafe impl Owned for Box<str> {
    type Borrowed = str;

    #[inline]
    fn owned_as_borrowed(&self) -> &Self::Borrowed { self }
}

unsafe impl DstKind for str {
    #[inline]
    fn extra_kind() -> ExtraKind { ExtraKind::Elements }
}

impl_common! { MooStr, str, Box<str>, String; basic_array, conv, comp }

impl<'a, 'b> From<MooStr<'a>> for Box<Error + Send + Sync + 'b> {
    fn from(v: MooStr<'a>) -> Self {
        String::from(v).into()
    }
}

impl<'a> From<MooStr<'a>> for Box<Error> {
    fn from(v: MooStr<'a>) -> Self {
        String::from(v).into()
    }
}

