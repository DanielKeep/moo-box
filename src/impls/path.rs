use std::borrow::Borrow;
use std::cmp::Ordering;
use std::path::{Path, PathBuf};
use ::Moo;
use ::traits::{Owned, DstKind, ExtraKind};

/**
A shorthand for `Moo<Box<Path>>`.
*/
pub type MooPath<'a> = Moo<'a, Box<Path>>;

unsafe impl Owned for Box<Path> {
    type Borrowed = Path;

    #[inline]
    fn owned_as_borrowed(&self) -> &Self::Borrowed { self }
}

unsafe impl DstKind for Path {
    #[inline]
    fn extra_kind() -> ExtraKind { ExtraKind::Elements }
}

impl_common! { MooPath, Path, Box<Path>, PathBuf; basic_array, conv, comp }

