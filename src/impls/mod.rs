macro_rules! impl_common {
    ($alias:ident, $bor:ident, $own:ty; $($macros:ident),*) => {
        $(
            $macros! { $alias, $bor, $own }
        )*
    };

    ($alias:ident, $bor:ident, $own:ty, $buf:ident; $($macros:ident),*) => {
        $(
            $macros! { $alias, $bor, $own, $buf }
        )*
    };
}

macro_rules! basic_array {
    ($alias:ident, $bor:ident, $own:ty, $buf:ident) => {
        impl<'a> Borrow<$bor> for $alias<'a> {
            fn borrow(&self) -> &$bor {
                self
            }
        }
    };
}

macro_rules! conv {
    ($alias:ident, $bor:ident, $own:ty) => {
        impl<'a> From<&'a $bor> for $alias<'a> {
            fn from(v: &'a $bor) -> Self {
                $alias::from_borrowed(v)
            }
        }

        impl<'a> From<$own> for $alias<'a> {
            fn from(v: $own) -> Self {
                $alias::from_owned(v)
            }
        }

        impl<'a> From<$alias<'a>> for $own {
            fn from(v: $alias<'a>) -> Self {
                v.into_owned()
            }
        }
    };

    ($alias:ident, $bor:ident, $own:ty, $buf:ident) => {
        conv! { $alias, $bor, $own }

        impl<'a> From<$buf> for $alias<'a> {
            fn from(v: $buf) -> Self {
                $alias::from_owned(<$own>::from(v))
            }
        }

        impl<'a> From<$alias<'a>> for $buf {
            fn from(v: $alias<'a>) -> Self {
                $buf::from(v.into_owned())
            }
        }
    };
}

macro_rules! comp {
    ($alias:ident, $bor:ident, $own:ty, $buf:ident) => {
        impl<'a> PartialEq<$bor> for $alias<'a> {
            fn eq(&self, other: &$bor) -> bool {
                (&**self).eq(other)
            }
        }

        impl<'a, 'b> PartialEq<&'b $bor> for $alias<'a> {
            fn eq(&self, other: &&'b $bor) -> bool {
                (&**self).eq(*other)
            }
        }

        impl<'a> PartialEq<$own> for $alias<'a> {
            fn eq(&self, other: &$own) -> bool {
                (&**self).eq(&**other)
            }
        }

        impl<'a> PartialEq<$buf> for $alias<'a> {
            fn eq(&self, other: &$buf) -> bool {
                (&**self).eq(&**other)
            }
        }

        impl<'a> PartialEq<$alias<'a>> for $bor {
            fn eq(&self, other: &$alias) -> bool {
                (&*self).eq(&**other)
            }
        }

        impl<'a, 'b> PartialEq<$alias<'a>> for &'b $bor {
            fn eq(&self, other: &$alias) -> bool {
                (&**self).eq(&**other)
            }
        }

        impl<'a> PartialEq<$alias<'a>> for $own {
            fn eq(&self, other: &$alias) -> bool {
                (&**self).eq(&**other)
            }
        }

        impl<'a> PartialEq<$alias<'a>> for $buf {
            fn eq(&self, other: &$alias) -> bool {
                (&**self).eq(&**other)
            }
        }

        impl<'a> PartialOrd<$bor> for $alias<'a> {
            fn partial_cmp(&self, other: &$bor) -> Option<Ordering> {
                (&**self).partial_cmp(other)
            }
        }

        impl<'a, 'b> PartialOrd<&'b $bor> for $alias<'a> {
            fn partial_cmp(&self, other: &&'b $bor) -> Option<Ordering> {
                (&**self).partial_cmp(*other)
            }
        }

        impl<'a> PartialOrd<$own> for $alias<'a> {
            fn partial_cmp(&self, other: &$own) -> Option<Ordering> {
                (&**self).partial_cmp(&**other)
            }
        }

        impl<'a> PartialOrd<$buf> for $alias<'a> {
            fn partial_cmp(&self, other: &$buf) -> Option<Ordering> {
                (&**self).partial_cmp(&**other)
            }
        }

        impl<'a> PartialOrd<$alias<'a>> for $bor {
            fn partial_cmp(&self, other: &$alias) -> Option<Ordering> {
                (&*self).partial_cmp(&**other)
            }
        }

        impl<'a, 'b> PartialOrd<$alias<'a>> for &'b $bor {
            fn partial_cmp(&self, other: &$alias) -> Option<Ordering> {
                (&**self).partial_cmp(&**other)
            }
        }

        impl<'a> PartialOrd<$alias<'a>> for $own {
            fn partial_cmp(&self, other: &$alias) -> Option<Ordering> {
                (&**self).partial_cmp(&**other)
            }
        }

        impl<'a> PartialOrd<$alias<'a>> for $buf {
            fn partial_cmp(&self, other: &$alias) -> Option<Ordering> {
                (&**self).partial_cmp(&**other)
            }
        }
    };
}

mod osstr;  pub use self::osstr::*;
mod path;   pub use self::path::*;
mod slice;  pub use self::slice::*;
mod str;    pub use self::str::*;
