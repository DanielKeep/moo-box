use std::borrow::Borrow;
use std::cmp::Ordering;
use ::Moo;
use ::traits::{Owned, DstKind, ExtraKind};

/**
A shorthand for `Moo<Box<[T]>>`.
*/
pub type MooVec<'a, T> = Moo<'a, Box<[T]>>;

unsafe impl<T> Owned for Box<[T]> {
    type Borrowed = [T];

    #[inline]
    fn owned_as_borrowed(&self) -> &Self::Borrowed { self }
}

unsafe impl<T> DstKind for [T] {
    #[inline]
    fn extra_kind() -> ExtraKind { ExtraKind::Elements }
}

impl<'a, T> Borrow<[T]> for MooVec<'a, T> {
    fn borrow(&self) -> &[T] {
        self
    }
}

impl<'a, T> From<&'a [T]> for MooVec<'a, T> {
    fn from(v: &'a [T]) -> Self {
        MooVec::from_borrowed(v)
    }
}

impl<'a, T> From<Box<[T]>> for MooVec<'a, T> {
    fn from(v: Box<[T]>) -> Self {
        MooVec::from_owned(v)
    }
}

impl<'a, T> From<Vec<T>> for MooVec<'a, T> {
    fn from(v: Vec<T>) -> Self {
        MooVec::from_owned(<Box<[T]>>::from(v))
    }
}

impl<'a, T> Into<Box<[T]>> for MooVec<'a, T>
where for<'b> &'b [T]: Into<Box<[T]>> {
    fn into(self) -> Box<[T]> {
        self.into_owned()
    }
}

impl<'a, T> Into<Vec<T>> for MooVec<'a, T>
where for<'b> &'b [T]: Into<Box<[T]>> {
    fn into(self) -> Vec<T> {
        Vec::from(self.into_owned())
    }
}

impl<'a, T> PartialEq<[T]> for MooVec<'a, T>
where T: PartialEq {
    fn eq(&self, other: &[T]) -> bool {
        (&**self).eq(other)
    }
}

impl<'a, 'b, T> PartialEq<&'b [T]> for MooVec<'a, T>
where T: PartialEq {
    fn eq(&self, other: &&'b [T]) -> bool {
        (&**self).eq(*other)
    }
}

impl<'a, T> PartialEq<Box<[T]>> for MooVec<'a, T>
where T: PartialEq {
    fn eq(&self, other: &Box<[T]>) -> bool {
        (&**self).eq(&**other)
    }
}

impl<'a, T> PartialEq<Vec<T>> for MooVec<'a, T>
where T: PartialEq {
    fn eq(&self, other: &Vec<T>) -> bool {
        (&**self).eq(&**other)
    }
}

impl<'a, T> PartialOrd<[T]> for MooVec<'a, T>
where T: PartialOrd {
    fn partial_cmp(&self, other: &[T]) -> Option<Ordering> {
        (&**self).partial_cmp(other)
    }
}

impl<'a, 'b, T> PartialOrd<&'b [T]> for MooVec<'a, T>
where T: PartialOrd {
    fn partial_cmp(&self, other: &&'b [T]) -> Option<Ordering> {
        (&**self).partial_cmp(*other)
    }
}

impl<'a, T> PartialOrd<Box<[T]>> for MooVec<'a, T>
where T: PartialOrd {
    fn partial_cmp(&self, other: &Box<[T]>) -> Option<Ordering> {
        (&**self).partial_cmp(&**other)
    }
}

impl<'a, T> PartialOrd<Vec<T>> for MooVec<'a, T>
where T: PartialOrd {
    fn partial_cmp(&self, other: &Vec<T>) -> Option<Ordering> {
        (&**self).partial_cmp(&**other)
    }
}
