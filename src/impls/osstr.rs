use std::borrow::Borrow;
use std::cmp::Ordering;
use std::ffi::{OsStr, OsString};
use ::Moo;
use ::traits::{Owned, DstKind, ExtraKind};

/**
A shorthand for `Moo<Box<OsStr>>`.
*/
/*
Is definitely *not* implemented by punching a cow mid-moo.
*/
pub type MooOs<'a> = Moo<'a, Box<OsStr>>;

unsafe impl Owned for Box<OsStr> {
    type Borrowed = OsStr;

    #[inline]
    fn owned_as_borrowed(&self) -> &Self::Borrowed { self }
}

unsafe impl DstKind for OsStr {
    #[inline]
    fn extra_kind() -> ExtraKind { ExtraKind::Elements }
}

impl_common! { MooOs, OsStr, Box<OsStr>, OsString; basic_array, conv, comp }

